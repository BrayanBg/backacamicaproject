FROM node:16-alpine

RUN mkdir -p /usr/src/back

WORKDIR /usr/src/back

COPY package*.json ./

RUN npm install
RUN npm i -g pm2@latest

COPY . .

CMD ["pm2-runtime","start","src/index.js","--name","sprint1","--watch","-i","2"]