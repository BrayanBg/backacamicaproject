const express = require('express');
const router = express.Router();
const passport = require('passport');
const strategyName = 'github';
const envior = require('../../utils/config');
const jwt = require('jsonwebtoken');
let urlFront = envior.URL_FRONT;
// eslint-disable-next-line array-element-newline
router.get('/auth', passport.authenticate(strategyName, { session: false, scope: ['user:email'] }));

router.get(
    '/callback',
    passport.authenticate(strategyName, {session:false, failureRedirect: `${urlFront}/failed`}),
    (req,res) => {
        // eslint-disable-next-line no-underscore-dangle
        const data = req.user._json;
        console.log(data);

        const token = jwt.sign({email: data.email,name: data.name,lastname: '_',isActive: true},envior.passToken);
        urlFront = `${envior.URL_BACK}/api?${token}`;
        res.redirect(301, `${urlFront}`);
    }
);


module.exports = router;