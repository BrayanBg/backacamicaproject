const express = require('express');
const router = express.Router();
const passport = require('passport');
const strategyName = 'google';
const envior = require('../../utils/config');
const jwt = require('jsonwebtoken');

let urlFront = envior.URL_FRONT;
// eslint-disable-next-line array-element-newline
router.get('/auth', passport.authenticate(strategyName, { session:false, scope: ['profile','email']}));

router.get(
    '/callback',
    passport.authenticate(strategyName, {session:false, failureRedirect: `${urlFront}/failed`}),
    (req,res) => {
        console.log(`Peticion get /${strategyName}/callback`);
        console.log(`Peticion get /${strategyName}/callback`);
        const data = req.user
        console.log('Data:')
        console.log(data);
        
        const token = jwt.sign({email: data.email,name: data.given_name,lastname: data.family_name,isActive: true},envior.passToken);
        urlFront = `${envior.URL_BACK}/api?${token}`;

        res.redirect(301, urlFront);
    }
);

module.exports = router;
