const express = require('express');
const router = express.Router();
const Product = require('../controllers/product.controller');
const extras = require('../middlewares/extra.middleware');

router.use(extras.errors)
router.use(extras.isActive)

router.get('/products', Product.get)

//Apartir de aqui solo administradores
router.use(extras.isAdmin)

router.post('/add', Product.add)
router.put('/edit', Product.edit)
router.delete('/delete', Product.deletes)

module.exports = router
