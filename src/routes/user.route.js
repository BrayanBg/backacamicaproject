const express = require('express');
const router = express.Router();
const User = require('../controllers/user.controller');
const extras = require('../middlewares/extra.middleware');

router.use(extras.errors)
router.use(extras.isActive)

router.put('/edit', User.edit) 

//Apartir de aqui solo administradores
router.use(extras.isAdmin)

router.get('/users', User.get)
router.delete('/delete', User.deletes)

module.exports = router
