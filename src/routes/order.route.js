const express = require('express');
const router = express.Router();
const Order = require('../controllers/order.controller');
const extras = require('../middlewares/extra.middleware');

router.use(extras.errors)
router.use(extras.isActive)

router.post('/add', Order.add)
router.get('/myOrders', Order.getMy)
router.put('/edit', Order.edit)

// Apartir de aqui solo usuarios admin deben acceder
router.use(extras.isAdmin)

router.get('/orders', Order.get)
router.put('/edit/admin', Order.editAdmin)

module.exports = router
