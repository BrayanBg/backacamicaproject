const express = require("express");
const mercadopago = require("./mercadopago");
const router = express.Router();

router.use("/mercadopago", mercadopago);

module.exports = router
