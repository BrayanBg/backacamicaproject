/* eslint-disable camelcase */
const express = require("express");
const router = express.Router();
const envior = require('../../utils/config');
const Product = require('../../DB/models/product.model');
const Order = require('../../DB/models/order.model');
const mercadopago = require("mercadopago");

// Agrega credenciales
mercadopago.configure({
  access_token: envior.MERCADOPAGO_TOKEN,
});

router.post("/pago", async (req, res) => {
  console.log('Pago');
  let busca = req.body;
  console.log(busca);
  let orde = await Order.findOne({id: busca.id});
  if(!orde){
    res.status(404).json('NOT FOUND');
  }
  const user = {
    id: 1097487433,
    name: "TETE3171440",
    password: "qatest1133",
    site_status: "active",
    email: "test_user_91730168@testuser.com",
  };

  let preference = {
    auto_return: "approved",
    back_urls: {
      success: `${envior.URL_BACK}/success`, 
      failure: `${envior.URL_BACK}/failure`, 
      pending: `${envior.URL_BACK}/pending`, 
    },
    payer: {
      name: user.name,
      surname: user.last_name,
      email: user.email,
    },
    items: await itemsStandar(orde),
  };

  mercadopago.preferences
    .create(preference)
    .then((response) => {
      console.log(response);
      let id = response.body.id;
      res.json({ preference_id: id, url: response.body.sandbox_init_point });
    })
    .catch((error) => {
      console.log(error);
    });
});

const itemsStandar =  async (order) =>{
  let items = [];
  for(let i = 0; i < order.products.length; i++ ){
      let pro = await Product.findOne({id: order.products[i].id});  
      let product = {
          title: pro.name,
          unit_price: pro.price,
          quantity:order.products[i].quantity
      }
      items.push(product);
  }
  console.log(items);
  return items;
}


module.exports = router;
