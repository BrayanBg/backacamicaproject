const express = require('express');
const expressJWT = require('express-jwt');

const envior = require('../utils/config');
const public = require('./public.routes');
const userRoutes = require('./user.route');
const productRoutes = require('./product.route');
const orderRoutes = require('./order.route');
const payRoutes = require('./payForm.route');
const login = require('./login');
const register = require('./register');
const payment = require('./payment');

const swaggerOptions = require('../utils/swaggerO');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);

const router = express();

router.use('/api', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));
router.use('/login', login);
router.use('/register', register);
router.use('/payment', payment);
router.use('/',public);

router.use(expressJWT({
    secret: envior.passToken,
    algorithms: ['HS256']
}));

router.use('/user', userRoutes);
router.use('/product', productRoutes);
router.use('/orders', orderRoutes);
router.use('/payform', payRoutes);


module.exports = router
