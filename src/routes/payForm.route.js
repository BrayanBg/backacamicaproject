const express = require('express');
const router = express.Router();
const Pay = require('../controllers/payform.controller');
const extras = require('../middlewares/extra.middleware');

router.use(extras.errors)
router.use(extras.isActive)

router.get('/payforms', Pay.get)

// Apartir de aqui solo usuarios admin deben acceder
router.use(extras.isAdmin)

router.post('/add', Pay.add)
router.put('/edit', Pay.edit)
router.delete('/delete', Pay.deletes)

module.exports = router
