const express = require("express");
const router = express.Router();
const google = require("./google");
const facebook = require("./facebook");
const github = require("./github.js");
const user = require('../../controllers/user.controller');

router.use('/google', google)
router.use('/facebook', facebook)
router.use('/github', github)
router.use('/add', user.add)

module.exports = router
