const express = require('express');
const router = express.Router();
const passport = require('passport');
const strategyName = 'facebook';
const envior = require('../../utils/config');
let urlFront = envior.URL_FRONT;
// eslint-disable-next-line array-element-newline
router.get('/auth', passport.authenticate(strategyName, { session: false, scope: ["email"] }));

router.get(
    `/callback`,
    passport.authenticate(strategyName, {session:false, failureRedirect: `${urlFront}/failed`}),
    (req,res) => {
        // eslint-disable-next-line no-underscore-dangle
        const data = req.user._json;
        console.log(data);

        const token = "hgjsd8fs6g7s7df67g6sdf43sdg2s3df5sg6s7df7";

        urlFront = `${envior.URL_FRONT}/?token=${token}`;
        res.redirect(301, `${urlFront}`);
    }
);


module.exports = router;
