const passport = require('passport');
const GitHubStrategy = require('passport-github2').Strategy;
const strategyName = 'github';
const envior = require('../utils/config');



passport.use(strategyName,new GitHubStrategy(
    {
    clientID: envior.GITHUB_CLIENT_ID,
    clientSecret: envior.GITHUB_CLIENT_SECRET,
    callbackURL: `${envior.URL_CALLBACK}${strategyName}/callback`
  },
  (accessToken, refreshToken, profile, done) => {
    /* 
     * User.findOrCreate({ googleId: profile.id }, function (err, user) {
     * return cb(err, user);
     *});
     */
    return done(null,profile);
  }
));