const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const strategyName = 'facebook';
const envior = require('../utils/config');



passport.use(strategyName,new FacebookStrategy(
    {
    clientID: envior.FACEBOOK_CLIENT_ID,
    clientSecret: envior.FACEBOOK_CLIENT_SECRET,
    callbackURL: `${envior.URL_CALLBACK}${strategyName}/callback`,
    profileFields: [
                    'id',
                    'displayName',
                    'email'
                  ]
  },
  (accessToken, refreshToken, profile, done) => {
    /* 
     * User.findOrCreate({ googleId: profile.id }, function (err, user) {
     * return cb(err, user);
     *});
     */
    return done(null,profile);
  }
));