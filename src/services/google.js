const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const strategyName = 'google';
const envior = require('../utils/config');



passport.use(strategyName,new GoogleStrategy(
    {
    clientID: envior.GOOGLE_CLIENT_ID,
    clientSecret: envior.GOOGLE_CLIENT_SECRET,
    callbackURL: `${envior.URL_CALLBACK}${strategyName}/callback`
  },
  (accessToken, refreshToken, profile, done) => {
    /* 
     * User.findOrCreate({ googleId: profile.id }, function (err, user) {
     * return cb(err, user);
     *});
     */
    return done(null,profile);
  }
));