/* eslint-disable no-lonely-if */
const Order = require('../DB/models/order.model');
const Product = require('../DB/models/product.model');
const orderValidation = require('../DB/schemas/order.schema');

const Orders = async (req, res) => {
    try {
        const orders = await Order.find();
        res.json(orders);
    } 
    catch (err) { 
        res.status(500).json(`Error: ${err}`);
    }  
}

const add = async (req, res) => {
    try{
        const order = req.body
        await orderValidation.validateAsync(order);
        let orders = await Order.find();
        order.id = orders.length + 1;
        order.email = req.user.email;
        order.total = await calTotal(order.products);
        order.status = 'InProgress';
        const aux = new Order(order);
        await aux.save();
        orders = await Order.findOne({id: order.id});
        res.status(201).json(orders);
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const calTotal = async (products) => {
    let total = 0;
    for(let product of products){
        let {price} = await Product.findOne({id: product.id});  
        total += product.quantity * price;
        console.log(total);
    }
    return total;
}

const edit = async (req, res) => {
    try{
        let orderE = req.body;
        let {status, email} = await Order.findOne({id: orderE.id});
        if(email!=req.user.email){
            res.status(401).json('This order belongs to another user');
        }
        if(status){
            if(status!='InProgress'){
                res.status(400).json('Order is closed');
            }
            orderE.email = req.user.email;
            orderE.total = await calTotal(orderE.products);
            orderE.status = status;
            await Order.updateOne({id: orderE.id}, orderE);
            orderE = await Order.findOne({id: orderE.id});
            res.status(200).json(orderE);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const editAdmin = async (req, res) => {
    try{
        let orderE = req.body;
        let {status} = await Order.findOne({id: orderE.id});
        if(status){
            if(status!='InProgress'){
                res.status(400).json('Order is closed');
            }
            orderE.email = req.user.email;
            orderE.total = await calTotal(orderE.products);
            await Order.updateOne({id: orderE.id}, orderE);
            orderE = await Order.findOne({id: orderE.id});
            res.status(200).json(orderE);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const get = async (req, res) => {
    let pays = await Order.find();
    res.json(pays);
}

const getMy = async (req, res) => {
    let pays = await Order.find({user: req.user.users});
    res.json(pays);
}

const getOrde = async (req,res) =>{
    let busca = req.body;
    let order = await Order.findOne({id: busca.id});
    res.json(order);
}

module.exports = { Orders, add, edit,  get, getMy, getOrde, editAdmin }
