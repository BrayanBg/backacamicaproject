const Product = require('../DB/models/product.model');
const productValidation = require('../DB/schemas/product.schema');

const Products = async (req, res) => {
    try {
        const product = await Product.find();
        res.json(product);
    } 
    catch (err) { 
        res.status(500).json(`Error: ${err}`);
    }  
}

const add = async (req, res) => {
    try{
        const product = req.body;
        await productValidation.validateAsync(product);
        let products = await Product.find();
        product.id =  products.length + 1;
        if(await Product.find({id: product.id})){
            product.id++;
        }
        const aux = new Product(product);
        await aux.save();
        res.status(201).json(product);
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }    
}

const edit = async (req, res) => {
    try{
        let products = req.body;
        let product = await Product.findOne({id: products.id});
        if(product){
            await Product.updateOne({id: products.id}, products);
            product = await Product.findOne({id: products.id});
            res.status(200).json(product);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const deletes = async (req, res) => {
    try{
        let busca = req.body;
        let p = await Product.findOne({id: busca.id});
        if(p){
            await Product.deleteOne({id: busca.id});
            res.json(`Product [${p.name}] deleted successfully`);
        }
        else res.status(404).json('NOT FOUND');
    }
    catch(err){
        res.status(400).json(err);
    }
}

const get = async (req, res) => {
    let products = await Product.find();
    res.json(products);
}

module.exports = { Products, add, get, deletes, edit }
