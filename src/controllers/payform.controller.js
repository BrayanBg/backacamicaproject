const Pay = require('../DB/models/payForm.model');
const payFormValidation = require('../DB/schemas/payForm.schema');

const Pays = async (req, res) => {
    try {
        const pays = await Pays.find();
        res.json(pays);
    } 
    catch (err) { 
        res.status(500).json(`Error: ${err}`);
    }  
}

const add = async (req, res) => {
    try{
        const payform = req.body
        await payFormValidation.validateAsync(payform);
        let payforms = await Pay.find();
        payform.id = payforms.length + 1;
        if(await Pay.find({id: payform.id})){
            payform.id++;
        }
        const aux = new Pay(payform);
        await aux.save();
        res.status(201).json(payform);
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const edit = async (req, res) => {
    try{
        let payforms = req.body;
        let pay = await Pay.findOne({id: payforms.id});
        if(pay){
            await Pay.updateOne({id: pay.id}, payforms);
            res.status(200).json(payforms);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const deletes = async (req, res) => {
    try{
        let busca = req.body;
        let pay = await Pay.findOne({id: busca.id});
        if(pay){
            await Pay.deleteOne({id: busca.id});
            res.json(`Pay Form [${pay.name}] deleted successfully`)
        }
        else{
            res.status(404).json('NOT FOUND');
        }
        
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const get = async (req, res) => {
    let pays = await Pay.find();
    res.json(pays);
}

module.exports = { Pays, add, edit, deletes, get }
