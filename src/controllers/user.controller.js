const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const envior = require('../utils/config');
const User = require('../DB/models/user.model');
const loginSchema = require('../DB/schemas/login.schema');
const userValidation = require('../DB/schemas/user.schema');
const userLogValidation = require('../DB/schemas/user.log.schema');

const Users = async (req, res) => {
    try {
        const user = await User.find();
        res.json(user);
    } 
    catch (err) { 
        res.status(500).json(`Error: ${err}`);
    }  
}

const loginUser = async (req, res) => {
    try{
        const {email , pass} = await loginSchema.validateAsync(req.body);
        let validate = await User.findOne({email});
        if(validate==null){
            res.status(404).json('NOT FOUND');
        }
        else{
            const {email: emails, password: passw, isActive, name, lastname} = validate;
            validate = bcrypt.compareSync(pass, passw);
            if(validate) {
                const token = jwt.sign({email: emails,name,lastname,isActive},envior.passToken);
                res.json(token);
            }
            else{
                res.status(400).json('INVALID CREDENTIALS');
            }
        }
     }
     catch(err){
        res.status(500).json(`Error: ${err}`);
     } 
}

const add = async (req, res) => {
    try{
        const salt = bcrypt.genSaltSync(10);
        const {email} = await userValidation.validateAsync(req.body);
        let validate = await User.findOne({email});
        if(validate){
            res.status(400).json('Email Existing');
        }
        else{
            let user = req.body;
            user.password = bcrypt.hashSync(user.password, salt);
            const aux = new User(user);
            await aux.save();
            user = await User.findOne({email});
            let resul = {
                name: user.name,
                lastname: user.lastname,
                direction: user.direction,
                email: user.email
            }
            res.status(201).json(resul);
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }     
}

const edit = async (req, res) => {
    try{
        let usee = req.body;
        const salt = bcrypt.genSaltSync(10)
        await userLogValidation.validateAsync(usee);
        if(await User.findOne({email: req.user.email})){
            let {isAdmin, isActive} = await User.findOne({email: req.user.email});
            usee.isAdmin = isAdmin;
            usee.activo = isActive;
            usee.email = req.user.email;
            usee.password = bcrypt.hashSync(usee.password, salt);
            await User.updateOne({email: usee.email}, usee);
            usee = await User.findOne({email: req.user.email});
            let resul = {
                name: usee.name,
                lastname: usee.lastname,
                direction: usee.direction,
                email: usee.email
            }
            res.status(200).json(resul);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
            
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const deletes = async (req, res) => {
    try{
        let use = req.body;
        if(await User.findOne({email: use.email})){
            await User.deleteOne({email: use.email});
            res.json(`User [${use.email}] delete successfully`);
        }
        else{
            res.status(404).json('NOT FOUND');
        }
    }
    catch(err){
        res.status(500).json(`Error: ${err}`);
    }
}

const get = async (req, res) => {
    let users = await User.find();
    res.json(users);
}

module.exports = { Users, add, loginUser, edit, deletes, get }
