const express = require('express');

const routes = require('./routes');

const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');

const app = express();
const port=3000;

const cspDefaults = helmet.contentSecurityPolicy.getDefaultDirectives();
delete cspDefaults['upgrade-insecure-requests'];
app.use(helmet({
    contentSecurityPolicy: { directives: cspDefaults }
}));

app.use(cors());

require('./DB');
require('./services');

app.use(express.json());

app.use(passport.initialize());

app.use('/',routes);

app.listen(port, () => { console.log(`Escuchando en el puerto ${port}`) });

module.exports = app
