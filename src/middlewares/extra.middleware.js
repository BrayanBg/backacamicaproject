const admin = require('./isAdmin.middleware');

// eslint-disable-next-line no-unused-vars
const errors = (err, req, res, next) => {
    if(err.name=='UnauthorizedError') res.status(401).json('User Unauthorized');
    else res.status(500).json('Server Error. [[[[[[SORRY]]]]]]');
}

const isActive = (req, res , next) =>{
    if (req.user.isActive) return next();
    else return res.status(401).json("User is not active");
};

const isAdmin = async (req, res, next) => {
    if (await admin(req.user.email)) return next();
    else return res.status(401).json("Is NOT ADMIN");
}

module.exports = { errors , isActive , isAdmin }