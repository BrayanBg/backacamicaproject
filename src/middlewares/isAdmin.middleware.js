const User = require('../DB/models/user.model');

const validarAdmin = async (user) => {
    const {isAdmin} = await User.findOne({email: user});
    return isAdmin;
}

module.exports = validarAdmin
