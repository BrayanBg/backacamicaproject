/* eslint-disable array-bracket-newline */
const mongoose = require('mongoose');

const ordersSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    formPay: {
        type: Number,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    products:{
        type: [{
            id: {
                type: Number,
                required: true
            },
            quantity: {
                type: Number,
                required: true
            },
        }],
        required: true
    },
    direction:{
        type: Number,
        require: true
    }
});

module.exports = mongoose.model('pedido', ordersSchema);