const mongoose = require('mongoose');

const usuariosSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true
    },
    direction: [
        {
            idDir:{
                type: Number,
                required: true
            },
            dir: {
                type: String,
                required: true
            },
        }
    ],
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('usuario', usuariosSchema);