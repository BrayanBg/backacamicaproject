const joi = require('joi');

const userValidation = joi.object({
    name: joi
            .string()
            .min(3)
            .required(),
    lastname: joi
            .string()
            .min(3)
            .required(),
    direction: joi
            .array().items(joi.object({idDir: joi.number(), dir: joi.string()}))
            .required(),
    password: joi
            .string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .required(),
    email: joi
            .string()
            .email({
                minDomainSegments: 2
            })
            .required(),
    isAdmin: joi.boolean(),
    isActive: joi.boolean()
});

module.exports = userValidation
