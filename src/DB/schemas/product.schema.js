const joi = require('joi');

const productValidation = joi.object({
    name: joi
        .string()
        .min(3)
        .required(),
    price: joi
        .number()
        .required(),
    description: joi
        .string()
        .min(3)
});

module.exports = productValidation
