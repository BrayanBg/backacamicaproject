const joi = require('joi');

const orderValidation = joi.object({
    formPay: joi
            .number()
            .required(),
    products: joi
            .array().items(joi.object({id: joi.number(), quantity: joi.number()})),
    direction: joi
            .number()
            .required(),
});

module.exports = orderValidation