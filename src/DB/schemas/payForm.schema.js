const joi = require('joi');

const payFormValidation = joi.object({
    name: joi
            .string()
            .min(3)
            .required(),
    description: joi
            .string()
            .min(3)
});

module.exports = payFormValidation
