/* eslint-disable no-undef */
const dotenv = require('dotenv');

dotenv.config();
const envior = {
    HOST: process.env.HOST,
    DB_NAME: process.env.DB_NAME,
    DB_PORT: process.env.DB_PORT,
    passToken: process.env.passToken,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    FACEBOOK_CLIENT_ID: process.env.FACEBOOK_CLIENT_ID,
    FACEBOOK_CLIENT_SECRET: process.env.FACEBOOK_CLIENT_SECRET,
    GITHUB_CLIENT_ID: process.env.GITHUB_CLIENT_ID,
    GITHUB_CLIENT_SECRET: process.env.GITHUB_CLIENT_SECRET,
    MERCADOPAGO_TOKEN: process.env.MERCADOPAGO_TOKEN,
    URL_CALLBACK: process.env.URL_CALLBACK,
    URL_FRONT: process.env.URL_FRONT,
    URL_BACK: process.env.URL_BACK
};

module.exports =  envior 