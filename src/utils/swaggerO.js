/* eslint-disable multiline-comment-style */
const swaggerOptions = {
    definition: {
        openapi: '3.0.1',
        info: {
          title: 'Sprint project - Protalento',
          description: 'Project for acamica DWBE by BBg',
          version: '4.0.0'
        },
        servers: [
          {
            url: 'https://api.acamica-bbg-restdelilah.ga',
            description: 'NGNIX server'
          },
          {
            url: 'http://localhost:3000/',
            description: '*'
          }
        ],
        tags: [
          {
            name: 'User',
            description: 'Everything about User'
          },
          {
            name: 'Product',
            description: 'Everything about Product'
          },
          {
            name: 'Orders',
            description: 'Everything about Orders'
          },
          {
            name: 'Pay Form',
            description: 'Everything about Pay Form'
          }
        ],
        paths: {
          '/login/log': {
            post: {
              tags: ['User'],
              summary: 'Login',
              requestBody: {
                description: 'Login object that needs to be Login',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/log'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'JWT - Json Web Token',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiVGVzdCIsIlRFU1QiOiJKV1QifQ.CMkzxUAa9NPvOKaRsDty5G1AzZqIAeg2Es_4Wgu8Rbk'
                      }
                    }
                  }
                },
                '400':{
                  description: 'INVALID CREDENTIALS',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'INVALID CREDENTIALS'
                      }
                    }
                  }
                },
                '404':{
                  description: 'NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'User NOT FOUND'
                      }
                    }
                  }
                }
              }
            }
          },
          '/register/add':{
            post:{
              tags: ['User'],
              summary: 'Register User',
              requestBody: {
                description: 'Json object that needs to be Register',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/user'
                    }
                  }
                },
                required: true
              },
              responses: {
                '201': {
                  description: 'User register successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/userRes'
                      }
                    }
                  }
                },
                '400': {
                  description: 'Email Existing',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Email Existing'
                      }
                    }
                  }
                }
              },
            }
          },
          '/user/users':{
            get:{
              tags: ['User'],
              summary: 'Get all Products (ONLY ADMIN)',
              responses: {
                '200': {
                  description: 'Get Users successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/users'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/user/edit': {
            put: {
              tags: ['User'],
              summary: 'Update User Account',
              requestBody: {
                description: 'Json object that needs to update User',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/usee'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Edited User successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/userRes'
                      }
                    }
                  }
                },
                '404': {
                  description: 'User NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'User NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/user/delete':{
            delete:{
              tags: ['User'],
              summary: 'Delete a User (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to delete a user',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/usser'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Deleted User successfully',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Deleted User successfully'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404':{
                  description: 'User NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'User NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/product/products':{
            get:{
              tags: ['Product'],
              summary: 'Get all Products',
              responses: {
                '200': {
                  description: 'Get Products successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/products'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/product/add':{
            post:{
              tags: ['Product'],
              summary: 'Add Product (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to add Product',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/product'
                    }
                  }
                },
                required: true
              },
              responses: {
                '201': {
                  description: 'Product added successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/products'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/product/edit':{
            put: {
              tags: ['Product'],
              summary: 'Update Product (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to update Product',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/products'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Edited Product successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/products'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Product NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Product NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/product/delete':{
            delete: {
              tags: ['Product'],
              summary: 'Delete Product (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to delete Product',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/busca'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Deleted Product successfully',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Deleted Product successfully'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Product NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Product NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/orders/add':{
            post:{
              tags: ['Orders'],
              summary: 'Add Order',
              requestBody: {
                description: 'Json object that needs to add Order',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/order'
                    }
                  }
                },
                required: true
              },
              responses: {
                '201': {
                  description: 'Order added successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/orders'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/orders/myOrders':{
            get:{
              tags: ['Orders'],
              summary: 'List My Orders',
              responses: {
                '200': {
                  description: 'List My Orders',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/orders'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/orders/orders':{
            get:{
              tags: ['Orders'],
              summary: 'Get all Orders (ONLY ADMIN)',
              responses: {
                '200': {
                  description: 'Get all Orders',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/orders'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/orders/edit/admin':{
            put: {
              tags: ['Orders'],
              summary: 'Update Order by admin (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to update Order',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/orderstatus'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Edited Order successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/orders'
                      }
                    }
                  }
                },
                '400': {
                  description: 'Order closed',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Order is closed'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Order NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Order NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/orders/edit':{
            put: {
              tags: ['Orders'],
              summary: 'Update Order',
              requestBody: {
                description: 'Json object that needs to update Order',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/orderE'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Edited Order successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/orders'
                      }
                    }
                  }
                },
                '400': {
                  description: 'Order closed',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Order is closed'
                      }
                    }
                  }
                },
                '401': {
                  description: 'Order from another user',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'This order belongs to another user'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Order NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Order NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/payform/payforms':{
            get:{
              tags: ['Pay Form'],
              summary: 'Get all Pay Forms',
              responses: {
                '200': {
                  description: 'Get Pay Forms successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/payforms'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/payform/add':{
            post:{
              tags: ['Pay Form'],
              summary: 'Add Pay Form (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to add Pay Form',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/payform'
                    }
                  }
                },
                required: true
              },
              responses: {
                '201': {
                  description: 'Pay Form added successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/payforms'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/payform/edit':{
            put: {
              tags: ['Pay Form'],
              summary: 'Update Pay Form (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to update Pay Form',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/payforms'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Edited Pay Form successfully',
                  content: {
                    'application/json': {
                      schema: {
                        '$ref': '#/components/schemas/payforms'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Pay Form NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Pay Form NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/payform/delete':{
            delete: {
              tags: ['Pay Form'],
              summary: 'Delete Pay Form (ONLY ADMIN)',
              requestBody: {
                description: 'Json object that needs to delete Pay Form',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/busca'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Deleted Pay Form successfully',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Deleted Pay Form successfully'
                      }
                    }
                  }
                },
                '401': {
                  description: 'User IS NOT ADMIN',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Is NOT ADMIN'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Pay Form NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Pay Form NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          },
          '/payment/mercadopago/pago': {
            post: {
              summary: 'MercadoPago',
              requestBody: {
                description: 'Integracion MercadoPago',
                content: {
                  'application/json': {
                    schema: {
                      '$ref': '#/components/schemas/busca'
                    }
                  }
                },
                required: true
              },
              responses: {
                '200': {
                  description: 'Compra aprobada',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: '{"preference_id": "1097481762-1f76b297-9866-4dc3-9925-3501bfc1ecb2",\n"url": "https://sandbox.mercadopago.com.co/checkout/v1/redirect?pref_id=1097481762-1f76b297-9866-4dc3-9925-3501bfc1ecb2"}'
                      }
                    }
                  }
                },
                '404': {
                  description: 'Pay Form NOT FOUND',
                  content: {
                    'application/json': {
                      schema: {
                        type: 'object',
                        example: 'Pay Form NOT FOUND'
                      }
                    }
                  }
                }
              },
              security: [
                {
                  'bearerAuth': []
                }
              ]
            }
          }
        },
        components: {
          schemas: {
            log: {
              type: 'object',
              properties: {
                email: {
                  type: 'string',
                  example: 'userTest@example.com',
                  description: 'User email'
                },
                pass: {
                  type: 'string',
                  example: 'pass',
                  description: 'User password'
                }
              }
            },
            userRes: {
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'User',
                  description: 'User name'
                },
                lastname: {
                  type: 'string',
                  example: 'Test',
                  description: 'User lastname'
                },
                direction: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      idDir: 1,
                      dir: 'Calle x Cra y N° z'
                    },
                    {
                      idDir: 2,
                      dir: 'Calle a Cra b N° c'
                    }
                  ],
                  description: 'User directions list'
                },
                email: {
                  type: 'string',
                  example: 'example@correo.com',
                  description: 'User email'
                }
              }
            },
            user: {
              type: 'object',
              properties: {
                email: {
                  type: 'string',
                  example: 'userTest@example.com',
                  description: 'User email'
                },
                password: {
                  type: 'string',
                  example: 'pass',
                  description: 'User passworsd'
                },
                name: {
                  type: 'string',
                  example: 'user',
                  description: 'User name'
                },
                lastname: {
                  type: 'string',
                  example: 'test',
                  description: 'User lastname'
                },
                direction: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      'idDir': 1,
                      'dir': 'Calle x Cra y N° z'
                    },
                    {
                      'idDir': 2,
                      'dir': 'Calle a Cra b N° c'
                    }
                  ],
                  description: 'User direction list'
                }
              }
            },
            users:{
              type: 'object',
              properties: {
                email: {
                  type: 'string',
                  example: 'userTest@example.com',
                  description: 'User email'
                },
                password: {
                  type: 'string',
                  example: 'X#23244AGDFHGRVWGBJSDG',
                  description: 'User passworsd'
                },
                name: {
                  type: 'string',
                  example: 'user',
                  description: 'User name'
                },
                lastname: {
                  type: 'string',
                  example: 'test',
                  description: 'User lastname'
                },
                direction: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      idDir: 1,
                      dir: 'Calle x Cra y N° z'
                    },
                    {
                      idDir: 2,
                      dir: 'Calle a Cra b N° c'
                    }
                  ],
                  description: 'User direction list'
                },
                isAdmin: {
                  type: 'boolean',
                  example: 'false',
                  description: 'True o false Is Admin?'
                },
                isActive: {
                  type: 'boolean',
                  example: 'false',
                  description: 'True o false Is active user?'
                }
              }
            },
            usee: {
              type: 'object',
              properties: {
                password: {
                  type: 'string',
                  example: 'pass',
                  description: 'User passworsd'
                },
                name: {
                  type: 'string',
                  example: 'user',
                  description: 'User name'
                },
                lastname: {
                  type: 'string',
                  example: 'test',
                  description: 'User lastname'
                },
                direction: {
                  type: 'array',
                  items: {
                    type: 'object',
                    example: {
                      idDir: 1,
                      dir: 'Calle x Cra y N° z'
                    }
                  },
                  example: [
                    {
                      idDir: 1,
                      dir: 'Calle x Cra y N° z'
                    },
                    {
                      idDir: 2,
                      dir: 'Calle a Cra b N° c'
                    }
                  ],
                  description: 'User direction list'
                }
              }
            },
            usser: {
              type: 'object',
              properties: {
                email: {
                  type: 'string',
                  example: 'userTest@example.com',
                  description: 'Email for delete User'
                },
              }
            },
            products: {
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'Product id'
                },
                name: {
                  type: 'string',
                  example: 'Agua',
                  description: 'Product name'
                },
                price:{
                  type: 'Number',
                  example: 2000,
                  description: 'Product price'
                },
                description:{
                  type: 'string',
                  example: 'Bottle water 1L',
                  description: 'Product description'
                }
              }
            },
            product: {
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'Agua',
                  description: 'Product name'
                },
                price:{
                  type: 'Number',
                  example: 2000,
                  description: 'Product price'
                },
                description:{
                  type: 'string',
                  example: 'Bottle water 1L',
                  description: 'Product description'
                }
              }
            },
            order:{
              type: 'object',
              properties: {
                formPay:{
                  type: 'Number',
                  example: 1,
                  description: 'id of form pay'
                },
                products: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      id: 1,
                      quantity: 1
                    },
                    {
                      id: 2,
                      quantity: 2
                    }
                  ],
                  description: 'Order product list'
                },
                direction:{
                  type: 'Number',
                  example: 1,
                  description: 'id of direction'
                },
              }
            },
            orders:{
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'Order id'
                },
                status: {
                  type: 'string',
                  example: 'InProgress',
                  description: 'Order status'
                },
                email: {
                  type: 'string',
                  example: 'userTest@example.com',
                  description: 'Email User '
                },
                total:{
                  type: 'Number',
                  example: 100000,
                  description: 'Total price of order'
                },
                formPay:{
                  type: 'Number',
                  example: 1,
                  description: 'id of form pay'
                },
                products: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      id: 1,
                      quantity: 1
                    },
                    {
                      id: 2,
                      quantity: 2
                    }
                  ],
                  description: 'Order product list'
                },
                direction:{
                  type: 'Number',
                  example: 1,
                  description: 'id of direction'
                },
              }
            },
            orderE:{
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'Order id'
                },
                formPay:{
                  type: 'Number',
                  example: 1,
                  description: 'id of form pay'
                },
                products: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      id: 1,
                      quantity: 1
                    },
                    {
                      id: 2,
                      quantity: 2
                    }
                  ],
                  description: 'Order product list'
                },
                direction:{
                  type: 'Number',
                  example: 1,
                  description: 'id of direction'
                },
              }
            },
            orderstatus:{
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'Order id'
                },
                status: {
                  type: 'string',
                  example: 'InProgress',
                  description: 'Order status'
                },
                formPay:{
                  type: 'Number',
                  example: 1,
                  description: 'id of form pay'
                },
                products: {
                  type: 'array',
                  items: {
                    type: 'object'
                  },
                  example: [
                    {
                      id: 1,
                      quantity: 1
                    },
                    {
                      id: 2,
                      quantity: 2
                    }
                  ],
                  description: 'Order product list'
                },
                direction:{
                  type: 'Number',
                  example: 1,
                  description: 'id of direction'
                },
              }
            },
            payform: {
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'Efectivo',
                  description: 'Pay form name'
                },
                description:{
                  type: 'string',
                  example: 'Pay with cash',
                  description: 'Pay form description'
                }
              }
            },
            payforms: {
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'Pay Form id'
                },
                name: {
                  type: 'string',
                  example: 'Efectivo',
                  description: 'Pay form name'
                },
                description:{
                  type: 'string',
                  example: 'Pay with cash',
                  description: 'Pay form description'
                }
              }
            },
            busca: {
              type: 'object',
              properties: {
                id:{
                  type: 'Number',
                  example: 1,
                  description: 'id'
                }
              }
            }
          },
          securitySchemes: {
            bearerAuth: {
              type: 'http',
              scheme: 'bearer',
              bearerFormat: 'JWT'
            }
          }
        }
      },
apis: ['./src/routes/*.js']
};

module.exports = swaggerOptions
