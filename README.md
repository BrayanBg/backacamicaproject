# Sprint project (Api sprint 4)

Proyecto en el cual podrás como administrador de un restaurante, agregar, modificar o quitar productos, ver los pedidos de tus clientes y modificar sus estados para organizarlos conforme se vayan preparando y como usuario podrás ver los productos disponibles, crear una cuenta, agregar o quitar productos de tu pedido, elegir medios de pago, especificar la dirección de entrega de tu pedido .

Este proyecto cuenta con dominio propio y esta siendo ejecutado con AWS utilizando los servicios

Para iniciar sesión en [Amazon Web Services](https://aws.amazon.com/es/console/) usa los datos adjuntos


## Recursos
- Node.js
- Express.js
- MongoDB Atlas
- JWT
- Swagger
- Bcrypt
- Joi
- EC2
- Router 53
- passport
- Docker
- #### IDPS
  - Google
  - Facebook
  - GitHub
- #### Pasarelas de pago
  - Mercado Pago

## Region para usuarios America del Norte
Region Norte de Virginia


## Ejecucion
#### 1. Lanzar instancia
```
Seleccionr AMI API
```
#### 3. Correr nginx
```
sudo nginx
```
#### 4. Correr servidor
```
pm2 start src/index.js --name sprint --watch -i 2
```

## A tener en cuenta

La base de datos se llena con unos datos semilla, en donde se llenan los productos, los usuarios por defecto y los metodos de pago.

Los ejemplos en las ordenes se pueden usar para el usuario cliente, para otras pruebas con otros usuarios, los ids de las direcciones se deben de cambiar.

El test se encuentra en la carpeta tests dentro de src.

En la sección métodos de pago, el endpoint **get Métodos de Pago** se encuentra abierto tanto para clientes como para los administradores, ya que estos dos son los que deben de tener la posibilidad de ver los métodos pago, no solo los administradores


## Documentation 
[Documentation](https://api.acamica-bbg-restdelilah.ga/api/) Se puede acceder en https://api.acamica-bbg-restdelilah.ga/api/

Usuarios:
```
| Usuario        | Contraseña  ||Rol          |
|----------------|-------------||-------------|
| admin          | admin       ||Administrador|
| user           | 123456      ||Usuario      |
```

## Inicio de session
[Inicio de Session](https://api.acamica-bbg-restdelilah.ga/api/) Se puede acceder en https://api.acamica-bbg-restdelilah.ga/api/
Se puede iniciar sesión por medio del EndPoint https://api.acamica-bbg-restdelilah.ga/register/add

## IDP 
Para validar la integración de IDPS debe acceder al siguiente link [Inicio de Session](https://acamica-bbg-restdelilah.ga)
el cual al loguearse se le redirigira al SWAGGER de la api.


## Mercado Pago
Se debe haber logeado con un token de acceso y se hara por medio del EndPoint https://api.acamica-bbg-restdelilah.ga/payment/mercadopago/pago

Para realizar un pago se debe ingresar el ID de una orden con minimo un producto.

se devolvera un objeto JSON de este estilo

```
{
  "preference_id": "1097481762-1f76b297-9866-4dc3-9925-3501bfc1ecb2",
  "url": "https://sandbox.mercadopago.com.co/checkout/v1/redirect?pref_id=1097481762-1f76b297-9866-4dc3-9925-3501bfc1ecb2"
}
```
copie la URL en su navegador para realizar el pago


#### Docker 
Para construir contenedor usar el comando
```
docker build -t [userDocker]/apirestdelilah:v1 .
```
Para ejecutar el proyecto a través de docker compose utilice el siguiente codigo:
```
docker-compose up -d
```


**\*Inicie sesión en la documentación de swagger con nombre de usuario y contraseña*